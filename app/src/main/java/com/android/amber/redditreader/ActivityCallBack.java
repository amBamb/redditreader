package com.android.amber.redditreader;

import android.net.Uri;

public interface ActivityCallBack {
    void onPostSelected(Uri link);
}
